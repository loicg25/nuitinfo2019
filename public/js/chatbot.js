var chatboxVisible = false;
var reponse3 = false;

function discution(){
	document.getElementById('bulleJC1P').innerHTML = "En quoi puis-je vous aider ?";
	document.getElementById('btnProp1').innerHTML = "Logement";
	document.getElementById('btnProp2').innerHTML = "Santé";
	document.getElementById('btnProp3').innerHTML = "Bourses";


}


document.addEventListener("DOMContentLoaded", function(){
	document.getElementById('barChatbox').addEventListener("click", function(){
		if(chatboxVisible){
			chatboxVisible = false;
			document.getElementById('chatbox').style.display = "none";
			document.getElementById('bulleUser').style.display = "none";
			document.getElementById('bulleJC2').style.display = "none";

		}else{
			chatboxVisible = true;
			document.getElementById('chatbox').style.display = "flex";
		}
		
	})


	document.getElementById('btnProp1').addEventListener("click", function(){
			document.getElementById('bulleUser').style.display = "block";
			document.getElementById('bulleUser').innerHTML = "Je veux des information sur les logements";
			document.getElementById('bulleJC2').style.display = "block";
			document.getElementById('bulleJC2').innerHTML = "Le crous propose des logements";
	
	})

	document.getElementById('btnProp2').addEventListener("click", function(){
		document.getElementById('bulleUser').style.display = "block";
		document.getElementById('bulleUser').innerHTML = "Je veux des informations pour ma santé";
		document.getElementById('bulleJC2').style.display = "block";
			document.getElementById('bulleJC2').innerHTML = "Prendre une mutuelle c'est important";
	})

	document.getElementById('btnProp3').addEventListener("click", function(){
		document.getElementById('bulleUser').style.display = "block";
		document.getElementById('bulleUser').innerHTML = "Je veux des informations sur les bourses";
		document.getElementById('bulleJC2').style.display = "block";
			document.getElementById('bulleJC2').innerHTML = "Il est important de faire une demande de bourse";
	})

	discution();
});
