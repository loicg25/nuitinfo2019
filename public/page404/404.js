window.onload = function () {


	//Global HTML elements
	var content = document.getElementById("content");
	var scoreText = document.getElementById("scoreText");
	var pointsText = document.getElementById("pointsText");

	// Global Game stuff
	//Stats
	var precarity; // 0 -> 100
	var score; //0 -> +Inf  (Timer)

	var finished = true;

	var precarityIncreaseMultiplier;
	var precarityDecreaseMultiplier;

	//Player
	class Player {

		constructor() {
            this.points = 0;//0 -> +Inf (Bonus)
            this.increasePoints(0);
        }

		increasePoints(amount) {

			if (amount < 0) {
				console.log("WARNING !! Amount = " + amount);
				return;
			}

			this.points += amount;
			pointsText.innerHTML = "Points: " + this.points;
		}

		getPoints() {
			return this.points;
		}

		purchased(cost) {

			if (cost <= 0) {
				console.log("WARNING !! Cost = " + cost);
				return;
			}

			if ((this.points - cost) < 0) {
				console.log("WARNING !! Points - cost = " + (this.points - cost));
				return;
			}

			this.points -= cost;
			pointsText.innerHTML = "Points: " + this.points;
		}

	}

	//Bonus
	class Bonus {
		constructor() {
			this.purchased = false;
			this.rate = 0;
			this.cost = 0;
		}

		purchase(player) {
			if (!this.purchased && player.getPoints() >= this.cost) {
				player.purchased(this.cost);
				this.purchased = true;
			}
		}

		isPurchased() {
			return this.purchased;
		}

	}

	//Popup
	class Popup {
		constructor(name, dropRate, timeBeforeDisappear, amount) {
			this.name = name;
			this.dropRate = dropRate;
			this.timeBeforeDisappear = timeBeforeDisappear;
			this.amount = amount;
			this.element = null;
			this.clicked = false;
		}

		tapped() {
			throw new Error("Try to call tapped from abstract class Popup");
		}

		getName() {
			return this.name;
		}

		getDropRate() {
			return this.dropRate;
		}

		getTimeBeforeDisappear() {
			return this.timeBeforeDisappear;
		}

		getAmount() {
			return this.amount;
		}

		spawn(player) {
			throw new Error("Try to call spawn from abstract class Popup");
		}

		hide() {
			if (this !== undefined && this !== null) {
				if (this.element !== undefined && this.element !== null) {
					content.removeChild(this.element);
					this.element = null;
					popups.splice(popups.indexOf(this), 1);
				}
			}
		}

		getRandomSpawnPosition(img) {
			let randX = Math.floor(Math.random() * (content.offsetWidth - (img.offsetWidth * 2) ) + img.offsetWidth);
			let randY = Math.floor(Math.random() * (content.offsetHeight - (img.offsetHeight * 2)) + img.offsetHeight);
			return { x: randX, y: randY };
		}

		apply(player) {
			throw new Error("Try to call apply from abstract class Popup");
		}

		spawnPopup(player, src, bonus) {
			let img = document.createElement("img");
			img.src = src;
			img.classList.add("popup");
			img.setAttribute("draggable", false);
			let pop = this;
			img.addEventListener("mousedown", function () {
				pop.tapped(player);
				increaseScore(bonus);
				pop.clicked = true;
			});
			content.appendChild(img);
			let position = this.getRandomSpawnPosition(img);
			img.style.left = position.x + "px";
			img.style.top = position.y + "px";
			this.element = img;
		}

	}
	class BonusPopup extends Popup {
		constructor(name, dropRate, timeBeforeDisappear, amount) {
			super(name, dropRate, timeBeforeDisappear, amount);
		}

		tapped(player) {
			this.apply(player);
		}

		apply(player) {
			if (this !== undefined && this !== null && !this.clicked) {
				player.increasePoints(this.amount);
				this.hide();
			}
		}

		spawn(player) {
			super.spawnPopup(player, "./img/popups/Bonus/" + this.name + ".png", 150);
			let pop = this;
			setTimeout(function () {
				pop.hide();
			}, this.timeBeforeDisappear);
		}

	}
	class MalusPopup extends Popup {
		constructor(name, dropRate, timeBeforeDisappear, amount) {
			super(name, dropRate, timeBeforeDisappear, amount);
		}

		tapped(player) {
			this.hide();
		}

		apply(player) {
			if (this !== undefined && this !== null && !this.clicked) {
				updatePrecarity(this.amount);
				this.hide();
			}
		}

		spawn(player) {
			super.spawnPopup(player, "./img/popups/Malus/" + this.name + ".png", 75);
			let pop = this;
			setTimeout(function () {
				pop.apply(player);
			}, this.timeBeforeDisappear);
		}

	}

	class Upgrade {
		constructor(popup, cost, precarityDecreaseMultiplier, factor) {
			this.bought = false;
			this.cost = cost;
			this.factor = factor;
			this.popup = popup;
			this.precarityDecreaseMultiplier = precarityDecreaseMultiplier;
		}

		buy(player, element) {
			if (!this.bought) {
				if (player.getPoints() >= this.cost) {
					this.bought = true;
					player.purchased(this.cost);
					precarityDecreaseMultiplier += this.precarityDecreaseMultiplier;
					if (this.popup.length > 0 && this.popup != "") {
						switch (this.popup) {
							case "ScholarshipsReduction":
								GoodMoodDropRate *= this.factor;
								break;
							case "HealthProblem":
								HealthProblemDropRate *= this.factor;
								break;
						}
					}
					element.classList.add("bought");
				}
			}
		}
	}

	//Player
	var player;

    var state; // 1 -> 75:100 | 2 -> 50:75 | 3 -> 25:50 | 4 -> 0:25

	//Popups
	var popups;
	var popupsPossibilities;

	var GoodMoodDropRate;
	var InheritanceDropRate;
    var FloodDropRate;
    var FireDropRate;
	var HealthProblemDropRate;
	var ScholarshipsReductionDropRate;

	var upgrades;

    var intervalId;

    tick();
    this.setInterval(function () {
        tick();
    }, 1000);

    startInterval(2000);

    function reset() {

        if (!finished) {
            return;
        }

        //Global HTML elements
        content = document.getElementById("content");
        scoreText = document.getElementById("scoreText");
        pointsText = document.getElementById("pointsText");

        // Global Game stuff
        //Stats
        precarity = 50; // 0 -> 100
        score = 0; //0 -> +Inf  (Timer)

        precarityIncreaseMultiplier = 1;
        precarityDecreaseMultiplier = 0;

        updatePrecarity(precarityIncreaseMultiplier - precarityDecreaseMultiplier);

        //Player
        player = new Player();

        state = 2 // 1 -> 75:100 | 2 -> 50:75 | 3 -> 25:50 | 4 -> 0:25

        //Popups
        popups = [];
        popupsPossibilities = [];
        //Adding all popups
        popupsPossibilities.push("GoodMood");
        popupsPossibilities.push("Inheritance");
        popupsPossibilities.push("Flood");
        popupsPossibilities.push("Fire");
        popupsPossibilities.push("HealthProblem");
        popupsPossibilities.push("ScholarshipsReduction");

        GoodMoodDropRate = 25;
        InheritanceDropRate = 15;
        FloodDropRate = 20;
        FireDropRate = 20;
        HealthProblemDropRate = 38;
        ScholarshipsReductionDropRate = 23;

        upgrades = {};
        upgrades["apl"] = new Upgrade("ScholarshipsReduction", 10, 1, 2);
        upgrades["soins"] = new Upgrade("HealthProblem", 20, 1, 0.5);
        upgrades["aide"] = new Upgrade("", 15, 1, 1);
        upgrades["alimentation"] = new Upgrade("", 15, 1, 1);

        let t = document.getElementsByClassName("bought");
        for (let i = 0; i < t.length; i++) {
            t[i].classList.remove("bought");
        }

    }

	// store in a function so we can call it again
	function startInterval(_interval) {
		// Store the id of the interval so we can clear it later
		intervalId = setInterval(function () {
			spawnPopups();
		}, _interval);
	}


    function spawnPopups() {

        if (finished) {
            return;
        }

        for (let i = 0; i < popupsPossibilities.length; i++) {
            if (popups.length >= 2) {
                return;
            }
			let popup = popupsPossibilities[i];
			let rand = Math.floor(Math.random() * 100);
			
			let pop;
			switch (popup) {
				case "GoodMood":
					pop = new BonusPopup("GoodMood", GoodMoodDropRate, 1500, 5);
					break;
				case "Inheritance":
					pop = new BonusPopup("Inheritance", InheritanceDropRate, 2000, 10);
					break;
				case "Flood":
					pop = new MalusPopup("Flood", FloodDropRate, 2500, 10);
                    break;
                case "Fire":
                    pop = new MalusPopup("Fire", FireDropRate, 2500, 10);
                    break;
				case "HealthProblem":
					pop = new MalusPopup("HealthProblem", HealthProblemDropRate, 2500, 15);
					break;
				case "ScholarshipsReduction":
					pop = new MalusPopup("ScholarshipsReduction", ScholarshipsReductionDropRate, 2000, 20);
					break;
			}
			if (rand < pop.getDropRate()) {
				popups.push(pop);
				pop.spawn(player);
			}
		}
	}

    function updateDropRate(newState) {

        if (finished) {
            return;
        }

		if (newState < state) {
			GoodMoodDropRate += 10;
			InheritanceDropRate += 10;
			FloodDropRate -= 10;
			HealthProblemDropRate -= 10;
			ScholarshipsReductionDropRate -= 10;
		}

		if (newState > state) {
			GoodMoodDropRate -= 15;
			InheritanceDropRate -= 15;
			FloodDropRate += 15;
			HealthProblemDropRate += 15;
			ScholarshipsReductionDropRate += 15;
		}
		
	}

	function tick() {
		if (finished) {
			return;
		}
		increaseScore(1);
		updatePrecarity(precarityIncreaseMultiplier - precarityDecreaseMultiplier);
	}

    function increaseScore(amount) {

        if (finished) {
            return;
        }

		score += amount;
		let scoreString = "";
		scoreText.innerHTML = "Score: ";
		if (score < 10) {
			scoreString += "0";
		}
		if (score < 100) {
			scoreString += "0";
		}
		if (score < 1000) {
			scoreString += "0";
		}
		if (score < 10000) {
			scoreString += "0";
		}
		scoreString += score;
		scoreText.innerHTML += scoreString;
	}

    function updatePrecarity(amount) {

        if (finished) {
            return;
        }

		if (precarity + amount >= 100) {
            document.getElementById("ending-screen").style.display = "block";
            document.getElementById("launch-screen").style.display = "block";
			document.getElementById("game").style.display = "none";
			document.getElementById("endTitle").innerHTML = "D&eacute;faite..";
            document.getElementById("endingMessage").innerHTML = "Score " + score;
			finished = true;
			return;
		}

		if (precarity + amount <= 0) {
            document.getElementById("ending-screen").style.display = "block";
            document.getElementById("launch-screen").style.display = "block";
			document.getElementById("game").style.display = "none";
			document.getElementById("endTitle").innerHTML = "Victoire!";
			document.getElementById("endingMessage").innerHTML = "Score " + score;
            finished = true;
			return;
		}

		precarity += amount;

		let newState;

		if (precarity < 25) {
			newState = 4;
		} else {
			if (precarity < 50) {
				newState = 3;
			} else {
				if (precarity < 75) {
					newState = 2;
				} else {
					newState = 1;
				}
			}
		}

		updateDropRate(newState);

		if (state != newState) {
			state = newState;
			// clear the existing interval
			clearInterval(intervalId);
			// just start a new one
			let newIntervalFrequence = 4000 / state;
			if (newIntervalFrequence < 1500) {
                newIntervalFrequence = 1500;
			}

			startInterval(newIntervalFrequence);
		}

		document.getElementById("precarityBar-value").style.width = precarity + "%";

		content.classList = "";

		if (precarity < 25) {
			content.classList = "Good";
		} else if (precarity < 50) {
			content.classList = "Rich";
		} else if (precarity < 75) {
			content.classList = "Medium";
		} else {
			content.classList = "OK";
		}

	}

	function buy(type, element) {
		if (upgrades[type] !== undefined && upgrades[type] !== null) {
			upgrades[type].buy(player, element);
		}
	}

	document.getElementById("apl").addEventListener("click", function (e) {
		buy("apl", e.target);
    });

    document.getElementById("clickToPlay").addEventListener("click", function (e) {
        reset();
        document.getElementById("ending-screen").style.display = "none";
        document.getElementById("launch-screen").style.display = "none";
        document.getElementById("game").style.display = "block";
        finished = false;
    });

	document.getElementById("soins").addEventListener("click", function (e) {
		buy("soins", e.target);
	});

	document.getElementById("aide").addEventListener("click", function (e) {
		buy("aide", e.target);
	});

	document.getElementById("alimentation").addEventListener("click", function (e) {
		buy("alimentation", e.target);
	});

}