const JC_BIENVENUE = "Bienvenue à toi sur dans les bureaux de l'administration !";
const JC_TUTO1 = ["Il paraît que tu viens pour faire valider ta bourse ?",
                "Il y a beaucoup de gens qui viennent comme toi en ce moment.",
                "Malheureusement pour toi, on dirait que ça va être un peu plus compliqué que prévu...",
                "Mais ne t'inquiète pas, je suis là pour t'aider !"];
const JC_TUTO_NOM = "Pour commencer, dis-moi comment tu t'appelles s'il te plaît";
const JC_TUTO2 = ["Ok, super " + " ! On va chercher ce papier ensemble !",
                  "Commence par voir si tu trouves quelque chose dans ce bureau."];



const JC_DEBUT = "Bon début, continue comme ça !";
const JC_BLACKOUT = "Oops, plus de lumière... On ne voit plus rien :/";
const JC_BESOIN_CODE = "Il nous faut le code de ce cadenas";
const JC_BESOIN_NUMERO = "Il y a beaucoup trop de dossiers là dedans... Il va nous falloir ton numéro ";
const JC_PASSWORD = "On dirait qu'on a besoin d'un mot de passe";
const JC_SPEEDUP = "Plus beaucoup de temps, grouillons-nous !";
const JC_FIN = "Bravo, tu as réussi à récupérer ton document de bourse ! Félicitations !";

function jeanCloudDitTuto(string) {
  document.getElementById("jeancloud").hidden = "true";
  document.getElementById("jeancloudgif").hidden = "";
  document.getElementById("jeancloudText").hidden = "";
  document.getElementById("jeancloudText").style= "width: 600%; height: 200%; top: -100%; font-size: 40px";
  document.getElementById("jeancloudText").innerHTML = string;
}

function jeanCloudDemandeNom() {
  jeanCloudDitTuto(JC_TUTO_NOM);
  document.getElementById("jeancloudText").innerHTML +=
  "<input id='entree_du_nom' type='text' name='nom' required>"
  +"<button onclick='validerNom()'>Valider le nom</button>";
}
