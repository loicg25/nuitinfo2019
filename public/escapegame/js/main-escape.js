var boutonMenu = document.getElementById("bouton-menu");
var boutonPause = document.getElementById("bouton-pause");
var boutonPlay = document.getElementById("bouton-play");
var menu = document.getElementById("menu");

function cleanTuto2() {
  document.getElementById("jeancloud").hidden = "";
  document.getElementById("jeancloudgif").hidden = "true";
  document.getElementById("jeancloudText").hidden = "true";
  document.getElementById("jeancloudText").innerHTML = "";
  jeancloudtext_end_time = 0;
}


function direTuto2(i) {
  jeanCloudDit(JC_TUTO2[i]);
  i++;
  if (JC_TUTO2[i] != undefined) {
    setTimeout(direTuto2, 2000);
  } else {
    setTimeout(cleanTuto2, 2000);
  }
};

/* Avancement dans le jeu */
document.getElementById("jeancloud").hidden = "";
document.getElementById("jeancloudgif").hidden = "true";
document.getElementById("jeancloudText").hidden = "true";
document.getElementById("jeancloudText").innerHTML = "";
jeancloudtext_end_time = 0
var clé_trouvée = false;
var mot_de_passe_trouvé = false;
var code_armoire_trouvé = false;
var numéro_caf_trouvé = false;
var dossier_trouvé = false;
var compteur_activable = false;
var nom = "test";
var numDossier = "42253620";
var clickCadre = 0;
var blackout = false;

/* Timer */
var time_in_minutes = 3;
var current_time = Date.parse(new Date());
var jeancloudtext_end_time = 0;

var interrupteur = document.getElementById("img-interrupteur");
var lumiereAllumee = true;

var deadline = new Date(current_time + time_in_minutes * 60 * 1000);

boutonPause.onclick = function() {
  menu.hidden = false;
  boutonPause.hidden = true;
  var compteur_boutons = [false, false, false, false];

  boutonPlay.hidden = false;
  pause_clock(deadline);
}

boutonPlay.onclick = function() {
  menu.hidden = true;
  boutonPause.hidden = false;
  boutonPlay.hidden = true;
  resume_clock(deadline);
}

function init() {
  afficherImages();
  lumiereAllumee = true;
  tiroirOuvert = false;

  // Tutoriel
  // jeanCloudGros();
  jeanCloudDitTuto(JC_BIENVENUE);

  var i = 0

  function direTuto1() {
    jeanCloudDitTuto(JC_TUTO1[i]);
    i++;
    if (JC_TUTO1[i]) {
      setTimeout(direTuto1, 2000);
    } else {
      setTimeout(jeanCloudDemandeNom(), 4000);
    }
  };
  direTuto1()


  run_clock('timer', deadline);
  document.getElementById("img-interrupteur").onclick = function() {
    if (lumiereAllumee) {
      eteindreLumiere("solid 145vw rgba(20, 20, 20, 0.8)", "-150vw", "-50vw");
    } else {
      allumerLumiere();
    }
  }
  document.getElementById("img-cadenas_tiroir").onclick = function() {
    if (clé_trouvée) {
      document.getElementById("img-cadenas_tiroir").hidden = "true";
      document.getElementById("img-bigten").hidden = "";
      ouvrirTiroir();
    }
  }

  document.getElementById("img-bigten").onclick = function() {
    document.getElementById("img-bigten").style = "height: 20%;left:50%;bottom:50%;";
    setTimeout(function() {
      document.onclick = function() {
        document.getElementById("img-bigten").hidden = "true";
      };
    }, 500);
  }

  document.getElementById("img-EG_tapis").onclick = function() {
    décalerTapis();
  }

  document.getElementById("img-EG_tableau").onclick = function() {
    if (clickCadre < 4) {
      bougerTableau();
      ++clickCadre;
    } else {
      tomberTableau();
    }
  }
  document.getElementById("jeancloudgif").onclick = function () {
    document.getElementById("jeancloud").hidden = "";
    document.getElementById("jeancloudgif").hidden = "true";
    document.getElementById("jeancloudText").hidden = "true";
    document.getElementById("jeancloudText").innerHTML = "";
    jeancloudtext_end_time = 0;
  }
  document.getElementById("img-EG_cle").onclick = function() {
    clé_trouvée = true;
    jeanCloudDit("Bon début !");
    document.getElementById("img-EG_cle").hidden = "true";
  }

  document.getElementById("compteur-electrique").onclick = function() {
    if (!isCompteurFullScreen) {
      compteurElectrique_fullscreen();
    }
  }

  document.getElementById("rouge").onclick = function() {
    if (isCompteurFullScreen && !compteur_activable) {
      compteur_boutons[0] = true;
      for (var i = 1; i < compteur_boutons.length; i++) {
        if (compteur_boutons[i]) {
          compteur_boutons = [false, false, false, false];
          presserBouttons();
          return;
        }
      }
    }
    presserBouttons();
  }

  document.getElementById("jaune").onclick = function() {
    if (isCompteurFullScreen && !compteur_activable) {
      for (var i = 0; i < 1; i++) {
        if (!compteur_boutons[i]) {
          compteur_boutons = [false, false, false, false];
          presserBouttons();
          return;
        }
      }
      compteur_boutons[1] = true;
      for (var i = 2; i < compteur_boutons.length; i++) {
        if (compteur_boutons[i]) {
          compteur_boutons = [false, false, false, false];
          presserBouttons();
          return;
        }
      }
    }
    presserBouttons();
  }

  document.getElementById("vert").onclick = function() {
    if (isCompteurFullScreen && !compteur_activable) {
      for (var i = 0; i < 2; i++) {
        if (!compteur_boutons[i]) {
          compteur_boutons = [false, false, false, false];
          presserBouttons();
          return;
        }
      }
      compteur_boutons[2] = true;
      for (var i = 3; i < compteur_boutons.length; i++) {
        if (compteur_boutons[i]) {
          compteur_boutons = [false, false, false, false];
          presserBouttons();
          return;
        }
      }
    }
    presserBouttons();
  }

  document.getElementById("bleu").onclick = function() {
    if (isCompteurFullScreen && !compteur_activable) {
      for (var i = 0; i < 3; i++) {
        if (!compteur_boutons[i]) {
          compteur_boutons = [false, false, false, false];
          presserBouttons();
          return;
        }
      }
      compteur_boutons[3] = true;
      compteur_activable = true;
      baisserLevier();
    }
    presserBouttons();
  }

  document.getElementById('quitter-compteur').onclick = function() {
    compteurElectrique_mur();
  }

  document.getElementById("img-EG_ecran1").onclick = function() {
    document.getElementById("screen").style.visibility = "visible";
  }

  document.getElementById("eteindreOrinateur").onclick = function() {
    document.getElementById("screen").style.visibility = "hidden";
    if (numéro_caf_trouvé) {
      document.getElementById("img-EG_ecran1").style.visibility = "hidden";
      document.getElementById("img-EG_ecran2").hidden = "";
    }
  }

  document.getElementById("img-EG_armoir_ferme").onclick = function() {
    if (!code_armoire_trouvé) {
      jeanCloudDit(JC_BESOIN_CODE);
    } else if (document.getElementById("img-EG_armoir_ouverte").hidden != "") {
      ouvrirArmoire();
    } else {
      if (!numéro_caf_trouvé) {
        jeanCloudDit(JC_BESOIN_NUMERO);
      } else {
        dossier_trouvé = true;
        jeanCloudDit(JC_FIN);
        pause_clock(deadline);
        document.getElementById("bourse").hidden = "";
      }
    }
  }

  document.getElementById("img-EG_armoir_ouverte").onclick = function() {
    if (!code_armoire_trouvé) {
      jeanCloudDit(JC_BESOIN_CODE);
    } else if (document.getElementById("img-EG_armoir_ouverte").hidden != "") {
      ouvrirArmoire();
    } else {
      if (!numéro_caf_trouvé) {
        jeanCloudDit(JC_BESOIN_NUMERO);
      } else {
        dossier_trouvé = true;
        jeanCloudDit(JC_FIN);
        pause_clock(deadline);
        document.getElementById("bourse").hidden = "";
      }
    }
  }
}

function afficherImages() {
  var images = document.getElementById("images");
  images.innerHTML = "";
  for (var image in imagesListe) {
    images.innerHTML +=
      '<label>' +
      '<img id="img-' + image + '" class="image" ' +
      'src="' + imagesListe[image].path + '" ' +
      'style="' + imagesListe[image].posX + imagesListe[image].posY + imagesListe[image].height + '"' +
      imagesListe[image].hiddenAuDebut +
      '></label>';
  }
}

function allumerLumiere() {
  if (compteur_activable) {
    blackout = false;
  }
  if (!blackout) {
    var ombre = document.getElementById("lumiere");
    ombre.style.display = "none";
    lumiereAllumee = true;
    document.getElementById("img-02778").hidden = "true";
    if (code_armoire_trouvé && !compteur_activable) {
      setTimeout(function() {
        blackout = true;
        eteindreLumiere("solid 145vw rgba(20, 20, 20, 0.8)", "-121vw", "-133vw");
      }, 2000);
    }
  }
}

function eteindreLumiere(border, left, top) {
  var ombre = document.getElementById("lumiere");
  ombre.style.left = left;
  ombre.style.top = top;
  ombre.style.border = border;
  ombre.style.display = "inline";
  lumiereAllumee = false;
  if (clickCadre < 4) {
    document.getElementById("img-02778").hidden = "true";
  } else {
    code_armoire_trouvé = true;
    document.getElementById("img-02778").hidden = "";
  }
}

function ouvrirTiroir() {
  document.getElementById("img-EG_tiroir_ferme").hidden = "true";
  document.getElementById("img-EG_tiroir_ouvert").hidden = "";
}


function décalerTapis() {
  let tapis = document.getElementById("img-EG_tapis");
  if (tapis.style.transform == "") {
    tapis.style.transform = "translate(-20%)";
  } else {
    tapis.style.transform = "";
  }
}

function ouvrirArmoire() {
  document.getElementById("img-EG_armoir_ferme").hidden = "true";
  document.getElementById("img-EG_armoir_ouverte").hidden = "";
}

var isCompteurFullScreen = false;

function compteurElectrique_fullscreen() {
  compteur_boutons = [false, false, false, false];
  presserBouttons();
  document.getElementById("compteur-electrique").style = "top: 10%; left: 40%; height: 66.825vh; width: 47.16vh;";
  if (compteur_activable) {
    document.getElementById("compteur-electrique").style.backgroundImage = "url('./assets/images/EG_micro_vert.png')"
  }
  isCompteurFullScreen = true;
  document.getElementById('quitter-compteur').hidden = "";
}

function compteurElectrique_mur() {
  isCompteurFullScreen = false;
  setTimeout(function() {
    document.getElementById("compteur-electrique").style = "top: 20vh; left: 25%; height: 22.275vh; width: 15.72vh;"
    document.getElementById('quitter-compteur').hidden = "true";
    if (compteur_activable) {
      document.getElementById("compteur-electrique").style.backgroundImage = "url('./assets/images/EG_micro_vert.png')"
    }
    isCompteurFullScreen = false;
  }, 50);
}

function bougerLevier() {
  if (document.getElementById("img-EG_action_haut").hidden != "") {
    leverLevier();
  } else {
    baisserLevier();
  }
}

function leverLevier() {
  document.getElementById("img-EG_action_bas").hidden = "true";
  document.getElementById("img-EG_action_haut").hidden = "";
  document.getElementById("compteur-electrique").style.backgroundImage = "url('./assets/images/EG_micro_rouge.png')"
}

function baisserLevier() {
  document.getElementById("img-EG_action_bas").hidden = "";
  document.getElementById("img-EG_action_haut").hidden = "true";
  document.getElementById("compteur-electrique").style.backgroundImage = "url('./assets/images/EG_micro_vert.png')"

}

function bougerTableau() {
  document.getElementById("img-EG_tableau").style.transform = "rotate(7deg)";
  setTimeout(function() {
    document.getElementById("img-EG_tableau").style.transform = "rotate(0deg)";
  }, 100);
}

function tomberTableau() {
  document.getElementById("img-EG_tableau").style.left = "20%";
  document.getElementById("img-EG_tableau").style.bottom = "20%";
  document.getElementById("img-EG_tableau").style.transform = "rotate(80deg)";
}

var compteur_boutons = [false, false, false, false];

function presserBouttons() {
  if (compteur_boutons[0]) {
    document.getElementById("rouge").style.backgroundColor = "green";
  } else {
    document.getElementById("rouge").style.backgroundColor = "rgba(0,0,0,0)";
  }

  if (compteur_boutons[1]) {
    document.getElementById("jaune").style.backgroundColor = "green";
    0
  } else {
    document.getElementById("jaune").style.backgroundColor = "rgba(0,0,0,0)";
  }

  if (compteur_boutons[2]) {
    document.getElementById("vert").style.backgroundColor = "green";
  } else {
    document.getElementById("vert").style.backgroundColor = "rgba(0,0,0,0)";
  }

  if (compteur_boutons[3]) {
    document.getElementById("bleu").style.backgroundColor = "green";
  } else {
    document.getElementById("bleu").style.backgroundColor = "rgba(0,0,0,0)";
  }
}

function deverrouiller() {
  if (!mot_de_passe_trouvé) {
    var mdp = document.getElementById("user-input").value;
    if (mdp == "BigTen") {
      mot_de_passe_trouvé = true;
      document.getElementById("sessionUtilisateur").style.display = "none";
      document.getElementById("screen-error").innerHTML = "";
      document.getElementById("screen-session").innerHTML = "Administration CROUS <br><br>Pour consulter le numéro de dossier,<br>veuillez entrer le nom de la personne...";
      document.getElementById("button-next").innerHTML = "[Consulter]";
      document.getElementById("user-input").value = "";
    } else {
      document.getElementById("screen-error").hidden = "";
      document.getElementById("screen-error").innerHTML = "Désolé, le mot de passe est incorrect.";
      document.getElementById("user-input").value = "";
    }
  } else {
    var nomInput = document.getElementById("user-input").value;
    if (nomInput == nom) {
      numéro_caf_trouvé = true;
      document.getElementById("sessionUtilisateur").style.display = "none";
      document.getElementById("screen-error").innerHTML = "";
      document.getElementById("screen-session").innerHTML = "Le numéro de dossier de " + nom + " est " + numDossier + ".";
      document.getElementById("button-next").style.display = "none";
      document.getElementById("user-input").style.display = "none";
    } else {
      document.getElementById("screen-error").hidden = "";
      document.getElementById("screen-error").innerHTML = "Désolé, ce nom n'existe pas.";
      document.getElementById("user-input").value = "";
    }
  }
}

function jeanCloudDit(string) {
  if (string == undefined) {
    cleanTuto2();
    return;
  }
  document.getElementById("jeancloud").hidden = "true";
  document.getElementById("jeancloudgif").hidden = "";
  document.getElementById("jeancloudText").hidden = "";
  document.getElementById("jeancloudText").style.width = " " + (string.length * 50) + "px;";
  document.getElementById("jeancloudText").innerHTML = string;
  jeancloudtext_end_time = Date.now() + (string.length * 1000);
}

function update_bubble_text() {
  if (jeancloudtext_end_time != 0 && jeancloudtext_end_time < Date.now()) {
    document.getElementById("jeancloud").hidden = "";
    document.getElementById("jeancloudgif").hidden = "true";
    document.getElementById("jeancloudText").hidden = "true";
    document.getElementById("jeancloudText").innerHTML = "";
    jeancloudtext_end_time = 0;
  }
}

function validerNom() {
  nom = document.getElementById('entree_du_nom').value;
  document.getElementById("jeancloud").hidden = "";
  document.getElementById("jeancloudgif").hidden = "true";
  document.getElementById("jeancloudText").hidden = "true";
  document.getElementById("jeancloudText").innerHTML = "";
  document.getElementById("jeancloudText").style = "width:300%; height: 60px; top: -20%; font-size: 20px";
  direTuto2(0)
}
