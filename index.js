//logging function 
function log(message){
    var options = {year: 'numeric', month: 'long', day: 'numeric' ,hour : 'numeric',minute: 'numeric'  ,second: 'numeric' };
    var date = new Date(Date.now())
    console.log(date.toLocaleString(options) + " : " + message)
 }

// Chargement des modules 
var express = require('express');
var mysql = require('mysql');
var dbUsersConnection ;

var app = express();
var server = app.listen(8080, function() {
    dbUsersConnection = mysql.createConnection({
        database : "ndi2019",
        host : "localhost",
        user : "dbuser",
        password : "dbUser"
    });
    dbUsersConnection.connect(function(err){
        if (err) console.log(err);
        console.log("Connected to the database.");
    });

    log("C'est parti ! En attente de connexion sur le port 80...");
});

// Configuration d'express pour utiliser le répertoire "public"
app.use(express.static('public'));
app.use('/404', express.static('public/page404'))
app.use('/chat', express.static('public/chat'))
app.use('/login', express.static('public/gestionUsers'))
app.use('/register', express.static('public/gestionUsers'))
app.use('/escapegame', express.static('public/escapegame'))


// set up to 
app.get('/', function(req, res) {  
    res.sendFile(__dirname + '/public/index.html');
});

app.get('/register', function(req, res) {  
    res.sendFile(__dirname + '/public/gestionUsers/register.html');
});

app.get('/login', function(req, res) {  
    res.sendFile(__dirname + '/public/gestionUsers/login.html');
});

app.get('/chat', function(req, res) {  
    
    res.sendFile(__dirname + '/public/chat/chat.html');
    
});
app.get('/escapegame', function(req, res) {  
    
    res.sendFile(__dirname + '/public/escapegame/escapegame.html');
    
});

app.get('/404', function(req, res) {  
    res.sendFile( '404.html', { root: __dirname + "/public/page404" });
    
});


app.post('/saveHighscore',function(req,res){

log(req)
res.send("hi");

})

app.use(function (req, res, next) {
    
    if (req.accepts('html')) {
        // Respond with html page.
        res.redirect('/404')
    }
    else if (req.accepts('json')) {
        // Respond with json.
        res.status(404).send({ error: 'Not found' });
    }
    else {
        // Default to plain-text. send()
        res.status(404).type('txt').send('Not found');
    }
  })

var io = require('socket.io')(server);


class Room {
    constructor(roomId){
        this.players = {};
        this.Room(roomId);
       }
       Room(roomId){    
        this.roomId = roomId
        log("new room created with id " + this.roomId + "key : " + this.roomKey);   
   
       }
       addPlayer(socket,clientId){
           if(!this.players[clientId])
        io.to(this.roomId).emit("message",this.roomId, { from: null, to: null,text: clientId + " a rejoint le jeu", date: Date.now() } );
        this.players[clientId] =  socket;
        this.players[clientId].join(this.roomId);
        this.players[clientId].emit("bienvenue", clientId);
        io.to(this.roomId).emit("liste", Object.keys(this.players));
       }
       sendMessage(msg){
        msg.date = Date.now();
        if(this.players[msg.from]){
            if(msg.to==null){
                io.to(this.roomId).emit("message", msg);
            }else{
                if(this.players[msg.to]!=null)
                    this.players[msg.to].emit("message", msg);
            }
            }
        }
        removePlayer(clientId){
            if(this.players[clientId]){
                this.players[clientId].leave(this.roomId); 
                io.to(this.roomId).emit("message", { from: null, to: null, text: clientId + " vient de se déconnecter de l'application", date: Date.now() });
                delete this.players[clientId]
                io.to(this.roomId).emit("liste", Object.keys(this.players));
            }
    
        }
    }
    
    

rooms = []
rooms.push(new Room("chatApl"));
rooms.push(new Room("chatBlabla1"));
rooms.push(new Room("chatBlabla2"));

/*** Gestion des clients et des connexions ***/
var clients = {};       // id -> socket

// Quand un client se connecte, on le note dans la console
io.on('connection', function (socket) {
    
    socket.on("register", function (ids) {
        let date = new Date();
        console.log("sending")
        if (dbUsersConnection.state !== 'disconnected') {
            let sql = "SELECT * FROM users WHERE username=" + dbUsersConnection.escape(ids.username);
            dbUsersConnection.query(sql, function (err, result) {
                if (err) throw err;
                if (result === undefined || result[0] === undefined || result[0].username === undefined) {
                    sql = "INSERT INTO users (username, password, email, date_register) VALUES(" + dbUsersConnection.escape(ids.username) + ", " + dbUsersConnection.escape(ids.password) + ", " +
                    dbUsersConnection.escape(ids.email) + ", " + dbUsersConnection.escape(date) + ")";
                    dbUsersConnection.query(sql, function (err, result) {
                        if (err) throw err;
                        socket.emit("register", { registered: true });
                        registered = true;
                    });
                } else {
                    socket.emit("register", { registered: false });//TODO Add more errors + types
                }
            });
        }
    });

    // message de debug
    log("Un client s'est connecté");
    var currentID = null;
    
    /**
     *  Doit être la première action après la connexion.
     *  @param  id  string  l'identifiant saisi par le client
     */
    socket.on("login", function(ids,roomid) {
        if (dbUsersConnection.state !== 'disconnected') {
            console.log(ids.username);
            id = null;
            let sql = "SELECT password FROM users WHERE username=" + dbUsersConnection.escape(ids.username);
            dbUsersConnection.query(sql, function (err, result) {
                if (err) throw err;
                if(result!=undefined && result!=null &&result[0]!=undefined &&result[0]!=null ){
                   if(result[0].password == ids.password)
                        id=ids.username;
                    else 
                        return
                }
       
        if(id!=null){
            console.log("here");
        room=null
        if(roomid == null){
            delete socket
            return
        }
        log(room)
        rooms.forEach
        (
            r=>{
                if(r.roomId==roomid)
                    room = r;
            }
        )
        if(room==null){
            delete socket
            return
        }
       
        currentID = id;
        room.addPlayer(socket,id)        
        log("Nouvel utilisateur : " + currentID);
    }

});
        }
    });
    
    
    /**
     *  Réception d'un message et transmission à tous.
     *  @param  msg     Object  le message à transférer à tous  
     */
    socket.on("message", function(msg,room) {
        log("Reçu message");   
        // si jamais la date n'existe pas, on la rajoute
        msg.date = Date.now();
        // si message privé, envoi seulement au destinataire
        if(msg.from !=null){
            log(" --> broadcast");
            rooms.forEach(room => {
                room.sendMessage(msg)
            });
         }else{
            log("Ignoring message because the sender is null ");   

         }
    });
    

    /** 
     *  Gestion des déconnexions
     */
    
    // fermeture
    socket.on("logout", function() { 
        // si client était identifié (devrait toujours être le cas)
        if (currentID) {
            log("Sortie de l'utilisateur " + currentID);
            // envoi de l'information de déconnexion
            socket.broadcast.emit("message", 
                { from: null, to: null, text: currentID + " a quitté la discussion", date: Date.now() } );
                // suppression de l'entrée
            delete clients[currentID];
            // envoi de la nouvelle liste pour mise à jour
            socket.broadcast.emit("liste", Object.keys(clients));
        }
    });
    
    // déconnexion de la socket
    socket.on("disconnect", function(reason) { 
        // si client était identifié
        if (currentID) {
            if (currentID) {
                // envoi de l'information de déconnexion
                rooms.forEach(room => {
                    room.removePlayer( currentID)
                });
            }
        }
        log("Client déconnecté");
    });
    
    
});

